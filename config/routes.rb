# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'static_pages#index'

  namespace :inspection do
    resources :results
    resources :histories do
      member do
        get 'pdf', as: :pdf
      end
    end
  end

  resources :areas do
    collection do
      get 'guide'
    end

    member do
      get 'map_image'
    end
  end
  resources :corporations

  resources :users do
    collection do
      get 'query'
    end
  end

  namespace :user do
    resources :operation_histories
  end

  namespace :equipment do
    resources :groups
    resources :components
    resources :meta_data do
      member do
        get 'download_pdf', as: :download_pdf
      end
    end
    resources :items do
      collection do
        get 'guide'
        get 'management'
        get 'create_inspection'
      end

      member do
        get 'map_image'
      end
      resources :inspection_histories, controller: '/inspection/histories'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #
  get 'sign_in', to: 'sessions#new', as: :new_session
  post 'sign_in', to: 'sessions#create', as: :create_session
  delete 'sign_out', to: 'sessions#destroy', as: :destroy_session

  scope controller: :api, path: '/api', as: :api do
    get 'get_layer2' => :get_layer2, as: :get_layer2
    get 'get_equ_component' => :get_equ_component, as: :get_equ_component
  end

  get 'qrcode_download', to: 'qrcode_download#index', as: :qrcode_download
end
