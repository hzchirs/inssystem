require 'test_helper'

class InspectionHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inspection_history = inspection_histories(:one)
  end

  test "should get index" do
    get inspection_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_inspection_history_url
    assert_response :success
  end

  test "should create inspection_history" do
    assert_difference('InspectionHistory.count') do
      post inspection_histories_url, params: { inspection_history: { equipment_id: @inspection_history.equipment_id, maintain_type: @inspection_history.maintain_type, results: @inspection_history.results, serial_number: @inspection_history.serial_number, uploaded_at: @inspection_history.uploaded_at, year: @inspection_history.year } }
    end

    assert_redirected_to inspection_history_url(InspectionHistory.last)
  end

  test "should show inspection_history" do
    get inspection_history_url(@inspection_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_inspection_history_url(@inspection_history)
    assert_response :success
  end

  test "should update inspection_history" do
    patch inspection_history_url(@inspection_history), params: { inspection_history: { equipment_id: @inspection_history.equipment_id, maintain_type: @inspection_history.maintain_type, results: @inspection_history.results, serial_number: @inspection_history.serial_number, uploaded_at: @inspection_history.uploaded_at, year: @inspection_history.year } }
    assert_redirected_to inspection_history_url(@inspection_history)
  end

  test "should destroy inspection_history" do
    assert_difference('InspectionHistory.count', -1) do
      delete inspection_history_url(@inspection_history)
    end

    assert_redirected_to inspection_histories_url
  end
end
