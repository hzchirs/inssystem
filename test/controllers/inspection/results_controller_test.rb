require 'test_helper'

class Inspection::ResultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inspection_result = inspection_results(:one)
  end

  test "should get index" do
    get inspection_results_url
    assert_response :success
  end

  test "should get new" do
    get new_inspection_result_url
    assert_response :success
  end

  test "should create inspection_result" do
    assert_difference('Inspection::Result.count') do
      post inspection_results_url, params: { inspection_result: { item_id: @inspection_result.item_id, result: @inspection_result.result } }
    end

    assert_redirected_to inspection_result_url(Inspection::Result.last)
  end

  test "should show inspection_result" do
    get inspection_result_url(@inspection_result)
    assert_response :success
  end

  test "should get edit" do
    get edit_inspection_result_url(@inspection_result)
    assert_response :success
  end

  test "should update inspection_result" do
    patch inspection_result_url(@inspection_result), params: { inspection_result: { item_id: @inspection_result.item_id, result: @inspection_result.result } }
    assert_redirected_to inspection_result_url(@inspection_result)
  end

  test "should destroy inspection_result" do
    assert_difference('Inspection::Result.count', -1) do
      delete inspection_result_url(@inspection_result)
    end

    assert_redirected_to inspection_results_url
  end
end
