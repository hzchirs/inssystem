require 'test_helper'

class Equipment::ComponentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment_component = equipment_components(:one)
  end

  test "should get index" do
    get equipment_components_url
    assert_response :success
  end

  test "should get new" do
    get new_equipment_component_url
    assert_response :success
  end

  test "should create equipment_component" do
    assert_difference('Equipment::Component.count') do
      post equipment_components_url, params: { equipment_component: { group_id: @equipment_component.group_id, name: @equipment_component.name } }
    end

    assert_redirected_to equipment_component_url(Equipment::Component.last)
  end

  test "should show equipment_component" do
    get equipment_component_url(@equipment_component)
    assert_response :success
  end

  test "should get edit" do
    get edit_equipment_component_url(@equipment_component)
    assert_response :success
  end

  test "should update equipment_component" do
    patch equipment_component_url(@equipment_component), params: { equipment_component: { group_id: @equipment_component.group_id, name: @equipment_component.name } }
    assert_redirected_to equipment_component_url(@equipment_component)
  end

  test "should destroy equipment_component" do
    assert_difference('Equipment::Component.count', -1) do
      delete equipment_component_url(@equipment_component)
    end

    assert_redirected_to equipment_components_url
  end
end
