require 'test_helper'

class Equipment::ItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment_item = equipment_items(:one)
  end

  test "should get index" do
    get equipment_items_url
    assert_response :success
  end

  test "should get new" do
    get new_equipment_item_url
    assert_response :success
  end

  test "should create equipment_item" do
    assert_difference('Equipment::Item.count') do
      post equipment_items_url, params: { equipment_item: { area_id: @equipment_item.area_id, metadata_id: @equipment_item.metadata_id, name: @equipment_item.name } }
    end

    assert_redirected_to equipment_item_url(Equipment::Item.last)
  end

  test "should show equipment_item" do
    get equipment_item_url(@equipment_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_equipment_item_url(@equipment_item)
    assert_response :success
  end

  test "should update equipment_item" do
    patch equipment_item_url(@equipment_item), params: { equipment_item: { area_id: @equipment_item.area_id, metadata_id: @equipment_item.metadata_id, name: @equipment_item.name } }
    assert_redirected_to equipment_item_url(@equipment_item)
  end

  test "should destroy equipment_item" do
    assert_difference('Equipment::Item.count', -1) do
      delete equipment_item_url(@equipment_item)
    end

    assert_redirected_to equipment_items_url
  end
end
