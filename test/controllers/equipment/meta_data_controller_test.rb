require 'test_helper'

class Equipment::MetaDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment_meta_datum = equipment_meta_data(:one)
  end

  test "should get index" do
    get equipment_meta_data_url
    assert_response :success
  end

  test "should get new" do
    get new_equipment_meta_datum_url
    assert_response :success
  end

  test "should create equipment_meta_datum" do
    assert_difference('Equipment::MetaDatum.count') do
      post equipment_meta_data_url, params: { equipment_meta_datum: { company: @equipment_meta_datum.company, component_id: @equipment_meta_datum.component_id, equ_model: @equipment_meta_datum.equ_model, fc_para: @equipment_meta_datum.fc_para } }
    end

    assert_redirected_to equipment_meta_datum_url(Equipment::MetaDatum.last)
  end

  test "should show equipment_meta_datum" do
    get equipment_meta_datum_url(@equipment_meta_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_equipment_meta_datum_url(@equipment_meta_datum)
    assert_response :success
  end

  test "should update equipment_meta_datum" do
    patch equipment_meta_datum_url(@equipment_meta_datum), params: { equipment_meta_datum: { company: @equipment_meta_datum.company, component_id: @equipment_meta_datum.component_id, equ_model: @equipment_meta_datum.equ_model, fc_para: @equipment_meta_datum.fc_para } }
    assert_redirected_to equipment_meta_datum_url(@equipment_meta_datum)
  end

  test "should destroy equipment_meta_datum" do
    assert_difference('Equipment::MetaDatum.count', -1) do
      delete equipment_meta_datum_url(@equipment_meta_datum)
    end

    assert_redirected_to equipment_meta_data_url
  end
end
