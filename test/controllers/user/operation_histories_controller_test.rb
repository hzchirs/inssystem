require 'test_helper'

class User::OperationHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_operation_history = user_operation_histories(:one)
  end

  test "should get index" do
    get user_operation_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_user_operation_history_url
    assert_response :success
  end

  test "should create user_operation_history" do
    assert_difference('User::OperationHistory.count') do
      post user_operation_histories_url, params: { user_operation_history: { action_type: @user_operation_history.action_type, note: @user_operation_history.note, operated_at: @user_operation_history.operated_at, user_id: @user_operation_history.user_id } }
    end

    assert_redirected_to user_operation_history_url(User::OperationHistory.last)
  end

  test "should show user_operation_history" do
    get user_operation_history_url(@user_operation_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_operation_history_url(@user_operation_history)
    assert_response :success
  end

  test "should update user_operation_history" do
    patch user_operation_history_url(@user_operation_history), params: { user_operation_history: { action_type: @user_operation_history.action_type, note: @user_operation_history.note, operated_at: @user_operation_history.operated_at, user_id: @user_operation_history.user_id } }
    assert_redirected_to user_operation_history_url(@user_operation_history)
  end

  test "should destroy user_operation_history" do
    assert_difference('User::OperationHistory.count', -1) do
      delete user_operation_history_url(@user_operation_history)
    end

    assert_redirected_to user_operation_histories_url
  end
end
