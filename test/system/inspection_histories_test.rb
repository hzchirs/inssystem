require "application_system_test_case"

class InspectionHistoriesTest < ApplicationSystemTestCase
  setup do
    @inspection_history = inspection_histories(:one)
  end

  test "visiting the index" do
    visit inspection_histories_url
    assert_selector "h1", text: "Inspection Histories"
  end

  test "creating a Inspection history" do
    visit inspection_histories_url
    click_on "New Inspection History"

    fill_in "Equipment", with: @inspection_history.equipment_id
    fill_in "Maintain type", with: @inspection_history.maintain_type
    fill_in "Results", with: @inspection_history.results
    fill_in "Serial number", with: @inspection_history.serial_number
    fill_in "Uploaded at", with: @inspection_history.uploaded_at
    fill_in "Year", with: @inspection_history.year
    click_on "Create Inspection history"

    assert_text "Inspection history was successfully created"
    click_on "Back"
  end

  test "updating a Inspection history" do
    visit inspection_histories_url
    click_on "Edit", match: :first

    fill_in "Equipment", with: @inspection_history.equipment_id
    fill_in "Maintain type", with: @inspection_history.maintain_type
    fill_in "Results", with: @inspection_history.results
    fill_in "Serial number", with: @inspection_history.serial_number
    fill_in "Uploaded at", with: @inspection_history.uploaded_at
    fill_in "Year", with: @inspection_history.year
    click_on "Update Inspection history"

    assert_text "Inspection history was successfully updated"
    click_on "Back"
  end

  test "destroying a Inspection history" do
    visit inspection_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Inspection history was successfully destroyed"
  end
end
