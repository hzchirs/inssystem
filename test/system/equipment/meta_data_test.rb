require "application_system_test_case"

class Equipment::MetaDataTest < ApplicationSystemTestCase
  setup do
    @equipment_meta_datum = equipment_meta_data(:one)
  end

  test "visiting the index" do
    visit equipment_meta_data_url
    assert_selector "h1", text: "Equipment/Meta Data"
  end

  test "creating a Meta datum" do
    visit equipment_meta_data_url
    click_on "New Equipment/Meta Datum"

    fill_in "Company", with: @equipment_meta_datum.company
    fill_in "Component", with: @equipment_meta_datum.component_id
    fill_in "Equ model", with: @equipment_meta_datum.equ_model
    fill_in "Fc para", with: @equipment_meta_datum.fc_para
    click_on "Create Meta datum"

    assert_text "Meta datum was successfully created"
    click_on "Back"
  end

  test "updating a Meta datum" do
    visit equipment_meta_data_url
    click_on "Edit", match: :first

    fill_in "Company", with: @equipment_meta_datum.company
    fill_in "Component", with: @equipment_meta_datum.component_id
    fill_in "Equ model", with: @equipment_meta_datum.equ_model
    fill_in "Fc para", with: @equipment_meta_datum.fc_para
    click_on "Update Meta datum"

    assert_text "Meta datum was successfully updated"
    click_on "Back"
  end

  test "destroying a Meta datum" do
    visit equipment_meta_data_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Meta datum was successfully destroyed"
  end
end
