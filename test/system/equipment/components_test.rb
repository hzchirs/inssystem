require "application_system_test_case"

class Equipment::ComponentsTest < ApplicationSystemTestCase
  setup do
    @equipment_component = equipment_components(:one)
  end

  test "visiting the index" do
    visit equipment_components_url
    assert_selector "h1", text: "Equipment/Components"
  end

  test "creating a Component" do
    visit equipment_components_url
    click_on "New Equipment/Component"

    fill_in "Group", with: @equipment_component.group_id
    fill_in "Name", with: @equipment_component.name
    click_on "Create Component"

    assert_text "Component was successfully created"
    click_on "Back"
  end

  test "updating a Component" do
    visit equipment_components_url
    click_on "Edit", match: :first

    fill_in "Group", with: @equipment_component.group_id
    fill_in "Name", with: @equipment_component.name
    click_on "Update Component"

    assert_text "Component was successfully updated"
    click_on "Back"
  end

  test "destroying a Component" do
    visit equipment_components_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Component was successfully destroyed"
  end
end
