require "application_system_test_case"

class Inspection::ResultsTest < ApplicationSystemTestCase
  setup do
    @inspection_result = inspection_results(:one)
  end

  test "visiting the index" do
    visit inspection_results_url
    assert_selector "h1", text: "Inspection/Results"
  end

  test "creating a Result" do
    visit inspection_results_url
    click_on "New Inspection/Result"

    fill_in "Item", with: @inspection_result.item_id
    fill_in "Result", with: @inspection_result.result
    click_on "Create Result"

    assert_text "Result was successfully created"
    click_on "Back"
  end

  test "updating a Result" do
    visit inspection_results_url
    click_on "Edit", match: :first

    fill_in "Item", with: @inspection_result.item_id
    fill_in "Result", with: @inspection_result.result
    click_on "Update Result"

    assert_text "Result was successfully updated"
    click_on "Back"
  end

  test "destroying a Result" do
    visit inspection_results_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Result was successfully destroyed"
  end
end
