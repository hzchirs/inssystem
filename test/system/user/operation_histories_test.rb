require "application_system_test_case"

class User::OperationHistoriesTest < ApplicationSystemTestCase
  setup do
    @user_operation_history = user_operation_histories(:one)
  end

  test "visiting the index" do
    visit user_operation_histories_url
    assert_selector "h1", text: "User/Operation Histories"
  end

  test "creating a Operation history" do
    visit user_operation_histories_url
    click_on "New User/Operation History"

    fill_in "Action type", with: @user_operation_history.action_type
    fill_in "Note", with: @user_operation_history.note
    fill_in "Operated at", with: @user_operation_history.operated_at
    fill_in "User", with: @user_operation_history.user_id
    click_on "Create Operation history"

    assert_text "Operation history was successfully created"
    click_on "Back"
  end

  test "updating a Operation history" do
    visit user_operation_histories_url
    click_on "Edit", match: :first

    fill_in "Action type", with: @user_operation_history.action_type
    fill_in "Note", with: @user_operation_history.note
    fill_in "Operated at", with: @user_operation_history.operated_at
    fill_in "User", with: @user_operation_history.user_id
    click_on "Update Operation history"

    assert_text "Operation history was successfully updated"
    click_on "Back"
  end

  test "destroying a Operation history" do
    visit user_operation_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Operation history was successfully destroyed"
  end
end
