# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_08_034145) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "area_categories", force: :cascade do |t|
    t.string "name"
    t.string "english_name"
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "area_codes", force: :cascade do |t|
    t.string "chinese_name"
    t.string "english_name"
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.bigint "owner_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "map_image_data"
    t.integer "category_id"
    t.integer "number"
    t.index ["owner_id"], name: "index_areas_on_owner_id"
  end

  create_table "corporations", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_corporations_on_name", unique: true
  end

  create_table "equipment_components", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "code"
    t.string "english_name"
    t.string "inspection_form_name"
  end

  create_table "equipment_groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_equipment_groups_on_name", unique: true
  end

  create_table "equipment_items", force: :cascade do |t|
    t.bigint "meta_datum_id"
    t.bigint "area_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "direction_code"
    t.text "map_image_data"
    t.string "note"
    t.index ["area_id"], name: "index_equipment_items_on_area_id"
    t.index ["meta_datum_id"], name: "index_equipment_items_on_meta_datum_id"
  end

  create_table "equipment_meta_data", force: :cascade do |t|
    t.bigint "component_id"
    t.string "company"
    t.string "equ_model"
    t.text "fc_para_data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["component_id"], name: "index_equipment_meta_data_on_component_id"
  end

  create_table "inspection_forms", force: :cascade do |t|
    t.bigint "equ_component_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["equ_component_id"], name: "index_inspection_forms_on_equ_component_id"
  end

  create_table "inspection_histories", force: :cascade do |t|
    t.bigint "equipment_item_id"
    t.integer "year"
    t.datetime "uploaded_at"
    t.integer "serial_number"
    t.string "maintain_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "results"
    t.index ["equipment_item_id"], name: "index_inspection_histories_on_equipment_item_id"
  end

  create_table "inspection_items", force: :cascade do |t|
    t.integer "form_id"
    t.string "content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["form_id"], name: "index_inspection_items_on_form_id"
  end

  create_table "inspection_photos", force: :cascade do |t|
    t.integer "result_id"
    t.text "image_data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["result_id"], name: "index_inspection_photos_on_result_id"
  end

  create_table "inspection_results", force: :cascade do |t|
    t.integer "item_id"
    t.integer "history_id"
    t.text "result"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["history_id"], name: "index_inspection_results_on_history_id"
    t.index ["item_id"], name: "index_inspection_results_on_item_id"
  end

  create_table "user_operation_histories", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "action_type"
    t.text "note"
    t.datetime "operated_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_user_operation_histories_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "password_digest", null: false
    t.bigint "corporation_id"
    t.string "role", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["corporation_id"], name: "index_users_on_corporation_id"
    t.index ["name"], name: "index_users_on_name", unique: true
  end

end
