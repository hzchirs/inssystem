# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

categories = Area::Category.create(Area::Category::LAYER1)
categories.each do |category|
  Area.create(name: category.name, category: category)
end

area_layer2_categories = [
  { name: '壩', english_name: 'Dam', code: 'D' },
  { name: '堰', english_name: 'Weir', code: 'W' },
  { name: '後池', english_name: 'Afterbay', code: 'A' },
  { name: '調整池', english_name: 'Auxiliary Lake', code: 'L' },
  { name: '圳路', english_name: 'Canal', code: 'C' },
  { name: '隧道', english_name: 'Tunnel', code: 'T' },
  { name: '溢洪道', english_name: 'Spillway', code: 'S' },
  { name: '分層取水工', english_name: 'Multilevel Intake', code: 'M' },
  { name: '發電廠', english_name: 'PowerPlant', code: 'P' },
  { name: '建築物', english_name: 'Building', code: 'B' }
]

Area::Category.create(area_layer2_categories)

components = [
  { name: '控制點', english_name: 'Control Point', code: 'CT' },
  { name: '沉陷位移觀測點', english_name: 'Settlement', code: 'ST' },
  { name: '壓力計', english_name: 'Pressure Cell', code: 'PC' },
  { name: '水壓計', english_name: 'Piezometers', code: 'PZ' },
  { name: '水位計', english_name: 'WaterLevel', code: 'WL' },
  { name: '滲漏量水堰', english_name: 'Seepage Weir', code: 'SP' },
  { name: '傾斜儀', english_name: 'Inclinometers', code: 'IC' },
  { name: '擺線儀', english_name: 'Pendulum', code: 'PD' },
  { name: '岩盤伸縮儀', english_name: 'Extensometer', code: 'EX' },
  { name: '應變計', english_name: 'Strain meters', code: 'SN' },
  { name: '強震儀', english_name: 'Seismograph', code: 'SM' },
  { name: '裂縫計', english_name: 'Joint', code: 'JT' },
  { name: '荷重計', english_name: 'Load Cell', code: 'LC' }
]

Equipment::Component.create(components)

# v12 
components2 = [
  { name: '雙頻光纖訊號增波器', english_name: 'Master Unit', code: 'Mu' },
  { name: '雙頻射頻訊號增波器', english_name: 'Remote Unit', code: 'Ru' },
  { name: '天線', english_name: 'Antenna', code: 'An' },
  { name: '不斷電系統', english_name: 'UPS', code: 'Up' },
  { name: '假負載', english_name: 'Dummy Load', code: 'Dl' },
  { name: '定點地圖', english_name: 'Map', code: 'Mp' }
]

Equipment::Component.create(components2)
