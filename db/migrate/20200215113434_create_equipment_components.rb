class CreateEquipmentComponents < ActiveRecord::Migration[6.0]
  def change
    create_table :equipment_components do |t|
      t.belongs_to :group
      t.string :name

      t.timestamps
    end
  end
end
