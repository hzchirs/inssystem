class CreateEquipmentItems < ActiveRecord::Migration[6.0]
  def change
    create_table :equipment_items do |t|
      t.belongs_to :metadata
      t.belongs_to :area
      t.string :name

      t.timestamps
    end
  end
end
