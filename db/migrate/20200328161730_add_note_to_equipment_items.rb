class AddNoteToEquipmentItems < ActiveRecord::Migration[6.0]
  def change
    add_column :equipment_items, :note, :string
  end
end
