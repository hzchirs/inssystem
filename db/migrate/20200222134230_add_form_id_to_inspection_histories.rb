class AddFormIdToInspectionHistories < ActiveRecord::Migration[6.0]
  def change
    add_column :inspection_histories, :form_id, :integer, null: false
    add_index :inspection_histories, :form_id
  end
end
