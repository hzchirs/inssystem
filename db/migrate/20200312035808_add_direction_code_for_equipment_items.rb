# frozen_string_literal: true

class AddDirectionCodeForEquipmentItems < ActiveRecord::Migration[6.0]
  def change
    add_column :equipment_items, :direction_code, :string
  end
end
