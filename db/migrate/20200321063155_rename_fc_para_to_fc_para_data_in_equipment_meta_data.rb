class RenameFcParaToFcParaDataInEquipmentMetaData < ActiveRecord::Migration[6.0]
  def change
    rename_column :equipment_meta_data, :fc_para, :fc_para_data
  end
end
