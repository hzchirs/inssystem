class ChangeFcParaToTextInEquipmentMetaData < ActiveRecord::Migration[6.0]
  def up
    change_column :equipment_meta_data, :fc_para, :text
  end

  def down
    change_column :equipment_meta_data, :fc_para, :string
  end
end
