class CreateEquipmentMetaData < ActiveRecord::Migration[6.0]
  def change
    create_table :equipment_meta_data do |t|
      t.belongs_to :component
      t.string :company
      t.string :equ_model
      t.string :fc_para

      t.timestamps
    end
  end
end
