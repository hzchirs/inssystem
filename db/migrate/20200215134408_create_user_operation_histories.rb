class CreateUserOperationHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :user_operation_histories do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.string :action_type
      t.text :note
      t.datetime :operated_at

      t.timestamps
    end
  end
end
