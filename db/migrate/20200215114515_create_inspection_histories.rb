class CreateInspectionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :inspection_histories do |t|
      t.belongs_to :equipment
      t.integer :year
      t.datetime :uploaded_at
      t.integer :serial_number
      t.string :maintain_type
      t.jsonb :results

      t.timestamps
    end
  end
end
