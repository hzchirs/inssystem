class RenameEquipmentItemsMetadataIdToMetaDatumId < ActiveRecord::Migration[6.0]
  def change
    rename_column :equipment_items, :metadata_id, :meta_datum_id
  end
end
