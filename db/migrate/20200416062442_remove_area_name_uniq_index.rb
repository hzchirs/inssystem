class RemoveAreaNameUniqIndex < ActiveRecord::Migration[6.0]
  def change
    remove_index :areas, name: 'index_areas_on_name'
  end
end
