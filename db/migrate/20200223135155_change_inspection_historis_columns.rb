# frozen_string_literal: true

class ChangeInspectionHistorisColumns < ActiveRecord::Migration[6.0]
  def change
    change_table :inspection_histories do |t|
      t.rename :equipment_id, :equipment_item_id
      t.remove :results
      t.remove :form_id
    end
  end
end
