class CreateAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :areas do |t|
      t.string :name, index: { unique: true }
      t.belongs_to :owner

      t.timestamps
    end
  end
end
