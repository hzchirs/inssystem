class CleanInspectionHistories < ActiveRecord::Migration[6.0]
  def change
    Inspection::History.find_each do |history|
      history.destroy if history.equipment_item.nil?
    end
  end
end
