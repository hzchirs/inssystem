class RemoveAdminAndUserRoleUsers < ActiveRecord::Migration[6.0]
  def change
    User.where(role: ['admin', 'user']).each(&:destroy)
  end
end
