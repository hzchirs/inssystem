class AddMapImageDataToAreas < ActiveRecord::Migration[6.0]
  def change
    add_column :areas, :map_image_data, :text
  end
end
