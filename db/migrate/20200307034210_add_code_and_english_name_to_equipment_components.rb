class AddCodeAndEnglishNameToEquipmentComponents < ActiveRecord::Migration[6.0]
  def change
    add_column :equipment_components, :code, :string
    add_column :equipment_components, :english_name, :string
  end
end
