# frozen_string_literal: true

class AddInspectionFormNameToEquipmentComponents < ActiveRecord::Migration[6.0]
  def change
    add_column :equipment_components, :inspection_form_name, :string
  end
end
