# frozen_string_literal: true

class CreateInspectionPhotos < ActiveRecord::Migration[6.0]
  def change
    create_table :inspection_photos do |t|
      t.integer :result_id, index: true
      t.text :image_data

      t.timestamps
    end
  end
end
