class CreateAreaCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :area_categories do |t|
      t.string :name
      t.string :english_name
      t.string :code

      t.timestamps
    end
  end
end
