# frozen_string_literal: true

class RemoveUserOperationHistoriesForeignKeyConstraint < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key 'user_operation_histories', 'users'
  end
end
