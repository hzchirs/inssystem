class RenameColumnFromCodeIdToCategoryIdOfAreas < ActiveRecord::Migration[6.0]
  def change
    rename_column :areas, :code_id, :category_id
  end
end
