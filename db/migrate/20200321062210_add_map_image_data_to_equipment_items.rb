class AddMapImageDataToEquipmentItems < ActiveRecord::Migration[6.0]
  def change
    add_column :equipment_items, :map_image_data, :text
  end
end
