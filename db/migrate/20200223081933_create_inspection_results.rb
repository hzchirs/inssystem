# frozen_string_literal: true

class CreateInspectionResults < ActiveRecord::Migration[6.0]
  def change
    create_table :inspection_results do |t|
      t.integer :item_id, index: true
      t.integer :history_id, index: true
      t.text :result

      t.timestamps
    end
  end
end
