class AddResultsToInspectionHistory < ActiveRecord::Migration[6.0]
  def change
    add_column :inspection_histories, :results, :jsonb
  end
end
