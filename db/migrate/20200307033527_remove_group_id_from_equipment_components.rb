class RemoveGroupIdFromEquipmentComponents < ActiveRecord::Migration[6.0]
  def change
    remove_column :equipment_components, :group_id, :integer
  end
end
