# frozen_string_literal: true

class MapImageUploader < Shrine
  def generate_location(io, record: nil, derivative: nil, **)
    return super unless record

    dir =
      if record.has_owner?
        '設施地圖'
      else
        '工區地圖'
      end

    "#{dir}/#{super}"
  end
end
