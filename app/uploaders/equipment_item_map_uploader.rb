# frozen_string_literal: true

class EquipmentItemMapUploader < Shrine
  def generate_location(io, record: nil, derivative: nil, **)
    return super unless record

    dir_name = '設備位置地圖'

    "#{dir_name}/#{super}"
  end
end
