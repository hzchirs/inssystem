# frozen_string_literal: true

class EquipmentFcParaUploader < Shrine
  def generate_location(io, record: nil, derivative: nil, **)
    return super unless record

    dir_name = '使用者手冊'
    dir = "#{dir_name}/#{record.component.name}"

    "#{dir}/#{super}"
  end
end
