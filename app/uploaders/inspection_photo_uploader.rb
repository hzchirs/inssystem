# frozen_string_literal: true

class InspectionPhotoUploader < Shrine
  def generate_location(io, record: nil, derivative: nil, **)
    return super unless record

    dir_name = '巡檢照片'

    "#{dir_name}/#{super}"
  end
end
