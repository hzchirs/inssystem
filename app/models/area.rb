# frozen_string_literal: true

# == Schema Information
#
# Table name: areas
#
#  id             :bigint           not null, primary key
#  map_image_data :text
#  name           :string
#  number         :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  category_id    :integer
#  owner_id       :bigint
#
# Indexes
#
#  index_areas_on_owner_id  (owner_id)
#
class Area < ApplicationRecord
  default_scope { order(id: :asc) }

  include MapImageUploader::Attachment(:map_image)

  has_many :equipment_items, class_name: 'Equipment::Item', dependent: :destroy
  has_many :sub_areas, class_name: 'Area', foreign_key: 'owner_id', dependent: :destroy

  belongs_to :category
  belongs_to :owner, class_name: 'Area', optional: true

  scope :layer1, -> { where(owner_id: nil) }
  scope :exclude_layer1, -> { where.not(id: layer1) }

  validates :name, uniqueness: { scope: [:owner] }

  delegate :layer1?, to: :category

  def hierachy
    has_owner? ? "#{owner&.name}-#{name}-#{number}" : name
  end

  def has_owner?
    owner_id.present?
  end

  def ch_type
    layer1? ? '工區' : '設施'
  end

  def equipment_components
    self.class.includes(sub_areas: [:equipment_items])
    if layer1?
      sub_areas.includes(equipment_items: [meta_datum: :component]).map(&:equipment_items).flatten.map(&:component).uniq
    else
      equipment_items.includes(meta_datum: :component).map(&:component).uniq
    end
  end
end
