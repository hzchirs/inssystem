# frozen_string_literal: true

class Inspection::Form
  LIST = [
    '暫不選取',
    '天線維運記錄',
    'UPS維運紀錄',
    'MU設備維運紀錄',
    'RU設備維運紀錄',
    '終端假負載維運紀錄'
  ].freeze

  attr_reader :name

  def initialize(name)
    raise ArgumentError unless name.in? LIST

    @name = name
  end

  def self.all
    LIST.map { |name| new(name) }
  end

  def self.find_by(name:)
    new(name)
  end
end
