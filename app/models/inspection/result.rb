# frozen_string_literal: true

# == Schema Information
#
# Table name: inspection_results
#
#  id         :bigint           not null, primary key
#  result     :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  history_id :integer
#  item_id    :integer
#
# Indexes
#
#  index_inspection_results_on_history_id  (history_id)
#  index_inspection_results_on_item_id     (item_id)
#
class Inspection::Result < ApplicationRecord
  belongs_to :history
  belongs_to :item

  has_many :photos, dependent: :destroy
  accepts_nested_attributes_for :photos, allow_destroy: true, reject_if: :all_blank
end
