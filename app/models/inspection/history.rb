# frozen_string_literal: true

# == Schema Information
#
# Table name: inspection_histories
#
#  id                :bigint           not null, primary key
#  maintain_type     :string
#  results           :jsonb
#  serial_number     :integer
#  uploaded_at       :datetime
#  year              :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  equipment_item_id :bigint
#
# Indexes
#
#  index_inspection_histories_on_equipment_item_id  (equipment_item_id)
#
class Inspection::History < ApplicationRecord
  belongs_to :equipment_item, class_name: 'Equipment::Item'

  before_create :assign_serial_number

  form_accessors = []

  # 表單欄位的名稱
  # A_0_1_1 ~ A_200_10_5
  (1..5).each do |i|
    (1..10).each do |j|
      form_accessors.concat((0..200).map { |k| :"A_#{k}_#{j}_#{i}" })
    end
  end

  # 相片欄位名稱
  # P_0_1, P_0_2 ~ P_200_1, P_200_2
  (1..2).each do |i|
    form_accessors.concat((0..200).map { |j| :"P_#{j}_#{i}" })
  end

  base_accessors = [
    :updates,
    :draft,
    :started_at,
    :completed_at
  ]

  store :results, coder: JSON, accessors: base_accessors.concat(form_accessors)

  def record_update(reason)
    updates << { reason: reason, at: Time.zone.now }
  end

  def pdf_path_hirachy
    File.join(
      equipment_item.area.owner.name,
      equipment_item.area.name,
      equipment_item.component.code
    )
  end

  def form_number
    generate_form_number unless results[:form_number]

    results[:form_number]
  end

  def generate_form_number
    results[:form_number] = "#{Time.zone.now.strftime('%Y%m%d')}_#{equipment_item.display_name}"
  end

  def pdf_filename
    "#{form_number}.pdf"
  end

  def archive_link_path
    '/' + (
      [
        'archives',
        '巡檢歷史紀錄PDF',
        pdf_path_hirachy,
        pdf_filename
      ].join('/')
    )
  end

  def archive_file_path
    Rails.root.join(
      'public',
      'archives',
      '巡檢歷史紀錄PDF',
      pdf_path_hirachy,
      pdf_filename
    )
  end

  def start
    generate_form_number
    self.updates = []
    self.draft = true
    self.started_at = Time.zone.now
  end

  def started?
    !started_at.nil?
  end

  def complete
    self.draft = false
    self.completed_at = Time.zone.now
  end

  def completed?
    !completed_at.nil?
  end

  private

  def assign_serial_number
    number = self.class.where(
      year: year,
      equipment_item: equipment_item
    ).maximum(:serial_number)

    self.serial_number = number ? number + 1 : 1
  end
end
