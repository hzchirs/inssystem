# frozen_string_literal: true

# == Schema Information
#
# Table name: inspection_photos
#
#  id         :bigint           not null, primary key
#  image_data :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  result_id  :integer
#
# Indexes
#
#  index_inspection_photos_on_result_id  (result_id)
#
class Inspection::Photo < ApplicationRecord
  include InspectionPhotoUploader::Attachment(:image)
end
