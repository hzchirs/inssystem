# == Schema Information
#
# Table name: corporations
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_corporations_on_name  (name) UNIQUE
#
class Corporation < ApplicationRecord
  validates :name, 
    presence: { message: '廠商名稱不可為空' },
    uniqueness: { message: '已經存在的廠商名稱' }

  has_many :users, dependent: :destroy
end
