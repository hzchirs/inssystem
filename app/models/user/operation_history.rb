# == Schema Information
#
# Table name: user_operation_histories
#
#  id          :bigint           not null, primary key
#  action_type :string
#  note        :text
#  operated_at :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :bigint           not null
#
# Indexes
#
#  index_user_operation_histories_on_user_id  (user_id)
#
class User::OperationHistory < ApplicationRecord
  belongs_to :user

  enum action_type: {
    sign_in: '登入',
    create_account: '新增帳號',
    change_password: '修改密碼',
    create: '資料新增',
    update: '資料修改',
    destroy: '資料刪除',
    upload_image: '上傳圖片'
  }, _prefix: :action

  ACTION_TYPE_MAPPING = {
    sign_in: '登入',
    create_account: '新增帳號',
    change_password: '修改密碼',
    create: '資料新增',
    update: '資料修改',
    destroy: '資料刪除',
    upload_image: '上傳圖片'
  }

  def action
    ACTION_TYPE_MAPPING[action_type&.to_sym]
  end
end
