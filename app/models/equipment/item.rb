# frozen_string_literal: true

# == Schema Information
#
# Table name: equipment_items
#
#  id             :bigint           not null, primary key
#  direction_code :string
#  map_image_data :text
#  name           :string
#  note           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  area_id        :bigint
#  meta_datum_id  :bigint
#
# Indexes
#
#  index_equipment_items_on_area_id        (area_id)
#  index_equipment_items_on_meta_datum_id  (meta_datum_id)
#
class Equipment::Item < ApplicationRecord
  include EquipmentItemMapUploader::Attachment(:map_image)

  belongs_to :meta_datum
  belongs_to :area

  has_many :inspection_histories,
           class_name: 'Inspection::History',
           foreign_key: 'equipment_item_id',
           dependent: :destroy,
           inverse_of: :equipment_item

  delegate :component, to: :meta_datum
  delegate :inspection_form, to: :component

  validate :display_name_can_not_duplicate

  def display_name_generatable?
    area.present?
  end

  def display_name
    [
      "#{area.owner.category.code}-#{area.category.code}",
      component.code,
      area.number,
      [name, direction_code.presence].compact.join('-')
    ].compact.join
  end

  def inspection_form?
    inspection_form && inspection_form.name != '暫不選取'
  end

  private

  def display_name_can_not_duplicate
    self.class.includes(
      area: [:category, owner: [:category]],
      meta_datum: [:component]
    ).find_each do |equ_item|
      next if equ_item.id == id

      if equ_item.display_name == display_name
        errors.add(:base, '設備唯一編碼重複')
      end
    end
  end
end
