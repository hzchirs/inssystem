# frozen_string_literal: true

# == Schema Information
#
# Table name: equipment_meta_data
#
#  id           :bigint           not null, primary key
#  company      :string
#  equ_model    :string
#  fc_para_data :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  component_id :bigint
#
# Indexes
#
#  index_equipment_meta_data_on_component_id  (component_id)
#
class Equipment::MetaDatum < ApplicationRecord
  include EquipmentFcParaUploader::Attachment(:fc_para)

  belongs_to :component, inverse_of: :meta_data

  has_many :items, dependent: :destroy

  def hierachy
    [
      component.name,
      company,
      equ_model
    ].join('-')
  end

  def fc_para_filename
    fc_para && fc_para.id.split('/')[2]
  end
end
