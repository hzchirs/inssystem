# frozen_string_literal: true

# == Schema Information
#
# Table name: equipment_components
#
#  id                   :bigint           not null, primary key
#  code                 :string
#  english_name         :string
#  inspection_form_name :string
#  name                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
class Equipment::Component < ApplicationRecord
  has_many :meta_data, dependent: :destroy
  has_many :items, through: :meta_data
  has_many :inspection_histories, through: :items

  def hierachy
    "#{name}(#{code})-#{inspection_form_name}"
  end

  def inspection_form
    Inspection::Form.find_by(name: inspection_form_name) if inspection_form_name
  end

  def layer2_areas
    items.includes(:area).map(&:area).uniq
  end
end
