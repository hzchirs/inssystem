# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  name            :string           not null
#  password_digest :string           not null
#  role            :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  corporation_id  :bigint
#
# Indexes
#
#  index_users_on_corporation_id  (corporation_id)
#  index_users_on_name            (name) UNIQUE
#
class User < ApplicationRecord
  has_secure_password

  SUPER_ADMIN = 'super_admin'
  ADMIN       = 'admin'
  USER        = 'user'
  VIEWER      = 'viewer'
  CORP_USER   = 'corp_user'

  CH_ROLES_MAPPING = {
    SUPER_ADMIN => '超級管理者',
    ADMIN => '管理者',
    USER => '使用者',
    VIEWER => '瀏覽者',
    CORP_USER => '廠商使用者'
  }.freeze

  enum role: {
    super_admin: SUPER_ADMIN,
    admin: ADMIN,
    user: USER,
    viewer: VIEWER,
    corp_user: CORP_USER
  }.freeze

  has_many :operation_histories, class_name: 'User::OperationHistory', dependent: :destroy
  belongs_to :corporation, optional: true

  validates :name,
            presence: { message: '使用者名稱不可為空' },
            uniqueness: { message: '已經存在的使用者名稱' }

  def ch_role
    CH_ROLES_MAPPING[role]
  end
end
