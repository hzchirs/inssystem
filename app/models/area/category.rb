# frozen_string_literal: true

# == Schema Information
#
# Table name: area_categories
#
#  id           :bigint           not null, primary key
#  code         :string
#  english_name :string
#  name         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Area::Category < ApplicationRecord
  LAYER1 = [
    { name: '石門', english_name: 'Shihmen', code: 'Sm' },
    { name: '中庄', english_name: 'Zhongzhuang', code: 'Zh' },
    { name: '羅東', english_name: 'Luodong', code: 'Ld' },
    { name: '榮華', english_name: 'Ronghua', code: 'Rh' },
    { name: '義興', english_name: 'Yising', code: 'Ys' },
    { name: '隆恩', english_name: 'Lungen', code: 'Le' },
    { name: '上坪', english_name: 'Shangping', code: 'Sp' },
    { name: '寶山第二水庫', english_name: 'Baoshan Second', code: 'BS' },
    { name: '桃園大圳', english_name: 'Taoyuan', code: 'Ty' },
    { name: '阿姆坪防淤隧道', english_name: 'Amuping', code: 'Am' },
    { name: '大灣坪防淤隧道', english_name: 'Dawanping', code: 'Dw' }
  ].freeze

  before_save :upcase_code

  has_many :areas, dependent: :destroy

  scope :layer1, -> { where(name: LAYER1.map { |layer1_category| layer1_category[:name] }) }
  scope :selectable, -> { where.not(id: layer1) }

  def layer1?
    name.in?(LAYER1.map { |category| category[:name] }) ||
      Area.find_by(name: name).present?
  end

  private

    def upcase_code
      code.upcase!
    end
end
