# frozen_string_literal: true

module ApplicationHelper
  def qrcode(url)
    RQRCode::QRCode.new(url).as_svg
  end
end
