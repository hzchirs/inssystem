module UsersHelper
  def role_options
    roles = User.roles.values

    roles = 
      if current_user.super_admin? || current_user.admin?
        roles.filter { |r| !r.in?(['admin', 'user']) }
      end

    roles.map { |r| [User::CH_ROLES_MAPPING[r], r] }
  end
end
