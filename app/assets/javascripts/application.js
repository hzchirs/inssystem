// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require bootstrap.min
//= require popper.min
//= require vue
//= require es6-promise.min
//= require es6-promise.auto.min
//= require axios.min
//= require jquery-3.3.1.js
//= require jquery.dataTables.min.js
//= require table2CSV.js
//= require dataTables.bootstrap4.min.js
//= require_tree .
