class UpdateArchivesService
  include Rails.application.routes.url_helpers

  def call
    Inspection::History.includes(equipment_item: [area: [:owner, :category], meta_datum: [:component]]).find_each do |history|
      next if File.file?(history.archive_file_path)

      create_archive(history) if history.completed?
    end
  end

  private

    def create_archive(inspection_history)
      pdf = WickedPdf.new.pdf_from_url(pdf_inspection_history_url(inspection_history, host: '10.56.136.234'), no_pdf_compression: false)

      relative_path = inspection_history.pdf_path_hirachy

      dir = Rails.root.join('public', 'archives', '巡檢歷史紀錄PDF', relative_path)

      FileUtils.mkdir_p(dir) unless File.directory?(dir)

      path = Rails.root.join(
        dir,
        inspection_history.pdf_filename
      )

      File.open(path, 'wb') do |file|
        file << pdf
      end
    end
end
