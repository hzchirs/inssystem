# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy]
  before_action :authenticate_admin!

  # GET /users
  # GET /users.json
  def index
    @corporations = Corporation.all
    @users = User.all
  end

  def query
    @users =
      if params[:role] == 'all'
        User.all
      else
        User.where(role: params[:role])
      end
  end

  # GET /users/1
  # GET /users/1.json
  def show; end

  # GET /users/new
  def new
    @user = User.new(corporation_id: params[:corporation_id])
  end

  # GET /users/1/edit
  def edit; end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        User::OperationHistory.create(
          user: current_user,
          action_type: :create_account,
          note: "新增帳號：#{@user.name}，權限：#{@user.role}"
        )
        format.html { redirect_to users_url, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      old_name = @user.name
      old_role = @user.ch_role

      if @user.update(user_params)
        if 'password_digest'.in? @user.saved_changes
          User::OperationHistory.create(
            user: current_user,
            action_type: :change_password,
            note: "修改使用者 #{@user.name} 密碼"
          )
        end

        changes = []

        changes << "名稱 #{old_name} -> #{@user.name}" if 'name'.in? @user.saved_changes
        changes << "角色 #{old_role} -> #{@user.ch_role}" if 'role'.in? @user.saved_changes

        note = changes.join('，')

        if note.present?
          User::OperationHistory.create(
            user: current_user,
            action_type: :update,
            note: '修改使用者：' + note
          )
        end

        format.html { redirect_to users_url, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    User::OperationHistory.create!(
      user: current_user,
      action_type: :destroy,
      note: "刪除使用者：#{@user.name}"
    )

    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation, :corporation_id, :role)
  end
end
