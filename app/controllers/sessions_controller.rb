class SessionsController < ApplicationController
  def new
    redirect_to root_path if user_signed_in?
  end

  def create
    user = User.find_by(name: params[:name])&.authenticate(params[:password])

    if user
      session[:user_id] = user.id

      User::OperationHistory.create(
        user: user,
        action_type: :sign_in,
        note: '登入帳號'
      )

      redirect_to root_url, notice: '登入成功'
    else
      flash.now[:alert] = '帳號或密碼錯誤'
      render action: :new
    end
  end

  def destroy
    session.delete(:user_id)

    redirect_to new_session_path, notice: '你已經成功登出'
  end
end
