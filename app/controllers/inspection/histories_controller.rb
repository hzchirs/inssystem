# frozen_string_literal: true

class Inspection::HistoriesController < ApplicationController
  ALL_ID = 9999

  before_action :set_inspection_history, only: %i[show pdf edit update destroy]

  # GET inspection/histories
  # GET inspection/histories.json
  def index
    cookies[:inspection_form_back_path] = request.path unless request.format.json?

    @layer1_areas = Area.select(:id, :name, :category_id).layer1

    if params[:layer1_area_id] && params[:layer2_area_id] && params[:equ_component_id]
      area = Area.find(params[:layer1_area_id])

      equ_items =
        if params[:layer2_area_id].to_i == ALL_ID
          area.sub_areas.map(&:equipment_items).flatten.uniq
        else
          Area.find(params[:layer2_area_id]).equipment_items.includes(:area, meta_datum: :component)
        end

      if params[:equ_component_id].to_i != ALL_ID
        equ_items = equ_items.filter do |item| 
          item.component.id == params[:equ_component_id].to_i
        end
      end

      inspection_histories = equ_items.map(&:inspection_histories).flatten.filter(&:started?)

      if params[:from] && params[:to]
        inspection_histories.filter! do |history|
          history.started_at.to_date.between?(params[:from].to_date, params[:to].to_date)
        end
      end


      respond_to do |format|
        format.json do
          render json: inspection_histories.map { |h|
            last_updated_at =
              if !h.updates.empty?
                h.updates.last[:at].to_datetime.strftime('%Y/%m/%d %H:%M:%S')
              elsif h.completed_at
                h.completed_at.to_datetime.strftime('%Y/%m/%d %H:%M:%S')
              else
                '未完成'
              end

            {
              id: h.id,
              layer1: area.name,
              layer2: h.equipment_item.area.name,
              component_name: h.equipment_item.component.name,
              has_fc_para: h.equipment_item.meta_datum.fc_para&.exists?,
              download_fc_para_url: download_pdf_equipment_meta_datum_path(h.equipment_item.meta_datum),
              year: h.year,
              serial_number: h.serial_number,
              form_number: h.form_number,
              started_at: h.started_at.to_datetime.strftime('%Y/%m/%d %H:%M:%S'),
              last_updated_at: last_updated_at
            }
          }
        end
      end
    end
  end

  # GET inspection/histories/1
  # GET inspection/histories/1.json
  def show
    @equipment_item = @inspection_history.equipment_item
    render "inspection/histories/forms/#{@inspection_history.equipment_item.inspection_form.name}.html.erb",
           layout: 'inspection_form'
  end

  def pdf
    @equipment_item = @inspection_history.equipment_item
    render "inspection/histories/forms/#{@inspection_history.equipment_item.inspection_form.name}.html.erb",
           layout: 'inspection_pdf'
  end

  # GET inspection/histories/new
  def new
    @equipment_item = Equipment::Item.find(params[:item_id])
    @inspection_history = @equipment_item.inspection_histories.build
    flash.now[:notice] = '若為新增巡檢表單請先點選右方『開始產製表單』'

    if @equipment_item.inspection_form?
      render "inspection/histories/forms/#{@equipment_item.inspection_form.name}.html.erb",
             layout: 'inspection_form'
    else
      render plain: '此設備尚未指定表單'
    end
  end

  # GET inspection/histories/1/edit
  def edit
    @inspection_history = Inspection::History.find(params[:id])
    @equipment_item = @inspection_history.equipment_item
    @mode = @inspection_history.completed_at ? 'update' : 'complete'

    render "inspection/histories/forms/#{@equipment_item.inspection_form.name}.html.erb",
           layout: 'inspection_form'
  end

  # POST inspection/histories
  # POST inspection/histories.json
  def create
    @inspection_history = Inspection::History.new(inspection_history_params)
    @inspection_history.start

    respond_to do |format|
      if @inspection_history.save
        format.html do
          redirect_to edit_inspection_history_url(@inspection_history),
                      notice: "開始巡檢，日期：#{@inspection_history.started_at.to_date}"
        end
        format.json { render :show, status: :created, location: @inspection_history }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT inspection/histories/1
  # PATCH/PUT inspection/histories/1.json
  def update
    @inspection_history.assign_attributes(inspection_history_params)

    upload_images

    if params[:complete]
      @inspection_history.complete
    elsif params[:update]
      @inspection_history.record_update(params[:reason])
    end

    respond_to do |format|
      if @inspection_history.save
        update_pdf_for(@inspection_history) unless params[:tmp]

        format.html do
          if params[:tmp]
            redirect_to edit_inspection_history_url(@inspection_history), notice: '已暫存'
          else
            redirect_to @inspection_history, notice: 'Inspection history was successfully updated.'
          end
        end

      else
        format.html { render :edit }
        format.json { render json: @inspection_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE inspection/histories/1
  # DELETE inspection/histories/1.json
  def destroy
    @inspection_history.destroy
    respond_to do |format|
      format.html do
        redirect_to inspection_histories_url, notice: 'Inspection history was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_inspection_history
    @inspection_history = Inspection::History.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def inspection_history_params
    params.require(:inspection_history).permit!
  end

  def upload_images
    (1..200).each do |m|
      (1..2).each do |n|
        column = "P_#{m}_#{n}"
        create_image_for(column) if inspection_history_params[column]
      end
    end
  end

  def update_pdf_for(inspection_history)
    pdf = WickedPdf.new.pdf_from_url(pdf_inspection_history_url(inspection_history), no_pdf_compression: false)

    relative_path = inspection_history.pdf_path_hirachy

    uploads_dir = Rails.root.join('public', 'uploads', '巡檢歷史紀錄PDF', relative_path)
    archives_dir = Rails.root.join('public', 'archives', '巡檢歷史紀錄PDF', relative_path)

    [uploads_dir, archives_dir].each do |dir|
      FileUtils.mkdir_p(dir) unless File.directory?(dir)

      path = Rails.root.join(
        dir,
        inspection_history.pdf_filename
      )

      File.open(path, 'wb') do |file|
        file << pdf
      end
    end
  end

  def create_image_for(column)
    return unless inspection_history_params[column]

    file_name = "#{column}.jpg"
    relative_path = File.join('uploads', '巡檢歷史紀錄照片', @inspection_history.id.to_s)

    dir = Rails.root.join('public', relative_path)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    path = Rails.root.join(dir, file_name)

    File.open(path, 'wb') do |file|
      file.write(inspection_history_params[column].read)
    end

    @inspection_history.results[column] = File.join('/', relative_path, file_name)
  end
end
