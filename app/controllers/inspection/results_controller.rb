class Inspection::ResultsController < ApplicationController
  before_action :set_inspection_result, only: [:show, :edit, :update, :destroy]

  # GET /inspection/results
  # GET /inspection/results.json
  def index
    @inspection_results = Inspection::Result.all
  end

  # GET /inspection/results/1
  # GET /inspection/results/1.json
  def show
  end

  # GET /inspection/results/new
  def new
    @inspection_result = Inspection::Result.new
  end

  # GET /inspection/results/1/edit
  def edit
  end

  # POST /inspection/results
  # POST /inspection/results.json
  def create
    @inspection_result = Inspection::Result.new(inspection_result_params)

    respond_to do |format|
      if @inspection_result.save
        format.html { redirect_to @inspection_result, notice: 'Result was successfully created.' }
        format.json { render :show, status: :created, location: @inspection_result }
      else
        format.html { render :new }
        format.json { render json: @inspection_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inspection/results/1
  # PATCH/PUT /inspection/results/1.json
  def update
    respond_to do |format|
      if @inspection_result.update(inspection_result_params)
        format.html { redirect_to @inspection_result, notice: 'Result was successfully updated.' }
        format.json { render :show, status: :ok, location: @inspection_result }
      else
        format.html { render :edit }
        format.json { render json: @inspection_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inspection/results/1
  # DELETE /inspection/results/1.json
  def destroy
    @inspection_result.destroy
    respond_to do |format|
      format.html { redirect_to inspection_results_url, notice: 'Result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inspection_result
      @inspection_result = Inspection::Result.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def inspection_result_params
      params.require(:inspection_result).permit(:item_id, :result)
    end
end
