# frozen_string_literal: true

class Equipment::MetaDataController < ApplicationController
  before_action :set_equipment_meta_datum, only: %i[show download_pdf edit update destroy]

  # GET /equipment/meta_data
  # GET /equipment/meta_data.json
  def index
    @equipment_meta_data = Equipment::MetaDatum.all
  end

  # GET /equipment/meta_data/1
  # GET /equipment/meta_data/1.json
  def show; end

  def download_pdf
    send_data @equipment_meta_datum.fc_para.read, filename: "#{@equipment_meta_datum.equ_model}.pdf"
  end

  # GET /equipment/meta_data/new
  def new
    @equipment_meta_datum = if params[:component_id]
                              Equipment::Component.find(params[:component_id]).meta_data.build
                            else
                              Equipment::MetaDatum.new
                            end
  end

  # GET /equipment/meta_data/1/edit
  def edit; end

  # POST /equipment/meta_data
  # POST /equipment/meta_data.json
  def create
    @equipment_meta_datum = Equipment::MetaDatum.new(equipment_meta_datum_params)

    respond_to do |format|
      if @equipment_meta_datum.save

        User::OperationHistory.create!(
          user: current_user,
          action_type: :create,
          note: "新增設備資訊：#{@equipment_meta_datum.hierachy}"
        )

        format.html { redirect_to @equipment_meta_datum.component, notice: 'Meta datum was successfully created.' }
        format.json { render :show, status: :created, location: @equipment_meta_datum }
      else
        format.html { render :new }
        format.json { render json: @equipment_meta_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equipment/meta_data/1
  # PATCH/PUT /equipment/meta_data/1.json
  def update
    old_hierachy = @equipment_meta_datum.hierachy

    respond_to do |format|
      if @equipment_meta_datum.update(equipment_meta_datum_params)
        if old_hierachy != @equipment_meta_datum.hierachy
          User::OperationHistory.create(
            user: current_user,
            action_type: :update,
            note: "修改設備資訊：#{old_hierachy} -> #{@equipment_meta_datum.hierachy}"
          )
        end

        if 'fc_para_data'.in?(@equipment_meta_datum.saved_changes.keys)
          User::OperationHistory.create(
            user: current_user,
            action_type: :upload_image,
            note: "更新 #{@equipment_meta_datum.hierachy} 的設備資訊手冊"
          )
        end

        format.html { redirect_to @equipment_meta_datum.component, notice: 'Meta datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @equipment_meta_datum }
      else
        format.html { render :edit }
        format.json { render json: @equipment_meta_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment/meta_data/1
  # DELETE /equipment/meta_data/1.json
  def destroy
    component = @equipment_meta_datum.component
    hierachy = @equipment_meta_datum.hierachy

    @equipment_meta_datum.destroy

    User::OperationHistory.create(
      user: current_user,
      action_type: :destroy,
      note: "刪除設備資訊：#{hierachy}"
    )
    respond_to do |format|
      format.html { redirect_to component, notice: 'Meta datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_equipment_meta_datum
    @equipment_meta_datum = Equipment::MetaDatum.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def equipment_meta_datum_params
    params.require(:equipment_meta_datum).permit(:component_id, :company, :equ_model, :fc_para)
  end
end
