# frozen_string_literal: true

class Equipment::ItemsController < ApplicationController
  ALL_ID = 9999

  before_action :set_equipment_item, only: %i[show edit update destroy]
  before_action :authenticate_admin!, except: %i[guide index create_inspection]

  # GET /equipment/items
  # GET /equipment/items.json
  def index
    if params[:get_full_data]
      area = Area.find(params[:layer1_area_id])
      equ_items = 
        if params[:layer2_area_id].to_i.zero?
          area.sub_areas.map(&:equipment_items).flatten.uniq
        else
          area.sub_areas.find_by(id: params[:layer2_area_id]).equipment_items
        end

      unless params[:equ_component_id].to_i.zero?
        equ_items = equ_items.filter { |item| item.component.id == params[:equ_component_id].to_i }
      end

      render json: equ_items.map { |item|
        {
          id: item.id,
          component_name: item.component.name,
          layer1: item.area.owner.name,
          layer2: item.area.name,
          display_name: item.display_name,
          model: item.meta_datum.equ_model,
          has_fc_para: item.meta_datum.fc_para&.exists?,
          download_fc_para_url: download_pdf_equipment_meta_datum_path(item.meta_datum),
          map_image_url: item.map_image_url ? (root_url + item.map_image_url).gsub('//', '/').gsub(':/', '://') : nil
        }
      } and return
    end

    if params[:layer1_area_id] && params[:layer2_area_id] && params[:equ_component_id]
      equ_items =
        if params[:layer2_area_id].to_i == ALL_ID
          Area.includes(:equipment_items).find(params[:layer1_area_id]).sub_areas.map(&:equipment_items).flatten.uniq
        else
          Area.find(params[:layer2_area_id]).equipment_items.includes(:area, meta_datum: :component)
        end

      if params[:equ_component_id].to_i != ALL_ID
        equ_items = equ_items.filter do |item| 
          item.component.id == params[:equ_component_id].to_i
        end
      end

      render json: equ_items.map { |item|
        {
          id: item.id,
          component_name: item.component.name,
          layer1: item.area.owner.name,
          layer2: item.area.name,
          display_name: item.display_name,
          model: item.meta_datum.equ_model,
          has_fc_para: item.meta_datum.fc_para&.exists?,
          download_fc_para_url: download_pdf_equipment_meta_datum_path(item.meta_datum),
          map_image_url: item.map_image_url ? (root_url + item.map_image_url).gsub('//', '/').gsub(':/', '://') : nil
        }
      } and return
    end

    @equipment_items = Equipment::Item.all
  end

  def create_inspection
    @layer1_areas = Area.select(:id, :name, :category_id).layer1
  end

  def guide
    @layer1_areas = Area.select(:id, :name, :category_id).layer1
    @layer2_areas = Area.select(:id, :name, :category_id).where(owner_id: @layer1_areas.first.id)
    @equ_components = @layer2_areas.map(&:equipment_components).flatten.uniq
  end

  def map_image
    item = Equipment::Item.find(params[:id])

    redirect_to item.map_image_url
  end

  def management
    @layer1_areas = Area.select(:id, :name, :category_id).layer1
    @layer2_areas = Area.select(:id, :name, :category_id).where(owner_id: @layer1_areas.first.id)
    @equ_components = @layer2_areas.map(&:equipment_components).flatten.uniq
  end

  # GET /equipment/items/1
  # GET /equipment/items/1.json
  def show
    cookies[:inspection_form_back_path] = request.path
    @inspection_histories = @equipment_item.inspection_histories
  end

  # GET /equipment/items/new
  def new
    @equipment_item =
      if params[:meta_datum_id]
        Equipment::MetaDatum.find(params[:meta_datum_id]).items.build
      else
        Equipment::Item.new
      end
  end

  # GET /equipment/items/1/edit
  def edit; end

  # POST /equipment/items
  # POST /equipment/items.json
  def create
    @equipment_item = Equipment::Item.new(equipment_item_params)

    render('new', equipment_item: @equipment_item) && return if params[:display_name]

    respond_to do |format|
      if @equipment_item.save
        User::OperationHistory.create!(
          user: current_user,
          action_type: :create,
          note: "新增設備：#{@equipment_item.display_name}"
        )
        format.html { redirect_to @equipment_item.component, notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @equipment_item }
      else
        format.html { render :new }
        format.json { render json: @equipment_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equipment/items/1
  # PATCH/PUT /equipment/items/1.json
  def update
    if params[:display_name]
      @equipment_item.assign_attributes(equipment_item_params)
      render('edit', equipment_item: @equipment_item) && return
    end

    respond_to do |format|
      old_display_name = @equipment_item.display_name

      if @equipment_item.update(equipment_item_params)
        if old_display_name != @equipment_item.display_name
          User::OperationHistory.create!(
            user: current_user,
            action_type: :update,
            note: "修改設備：#{old_display_name} -> #{@equipment_item.display_name}"
          )
        end

        if 'map_image_data'.in? @equipment_item.saved_changes.keys
          User::OperationHistory.create!(
            user: current_user,
            action_type: :upload_image,
            note: "更新 #{@equipment_item.display_name} 的地圖"
          )
        end

        format.html { redirect_to @equipment_item.component, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @equipment_item }
      else
        format.html { render :edit }
        format.json { render json: @equipment_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment/items/1
  # DELETE /equipment/items/1.json
  def destroy
    User::OperationHistory.create!(
      user: current_user,
      action_type: :destroy,
      note: "刪除設備：#{@equipment_item.display_name}"
    )
    @equipment_item.destroy
    respond_to do |format|
      format.html { redirect_to @equipment_item.component, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_equipment_item
    @equipment_item = Equipment::Item.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def equipment_item_params
    params.require(:equipment_item).permit(:meta_datum_id, :area_id, :name, :direction_code, :map_image, :note)
  end
end
