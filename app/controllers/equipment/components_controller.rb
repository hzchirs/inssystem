# frozen_string_literal: true

class Equipment::ComponentsController < ApplicationController
  before_action :set_equipment_component, only: %i[show edit update destroy]

  # GET /equipment/components
  # GET /equipment/components.json
  def index
    @equipment_components = Equipment::Component.all
  end

  # GET /equipment/components/1
  # GET /equipment/components/1.json
  def show
    @meta_data = @equipment_component.meta_data
    @equipment_items = @equipment_component.items.includes(:area)
  end

  # GET /equipment/components/new
  def new
    @equipment_component = Equipment::Component.new
  end

  # GET /equipment/components/1/edit
  def edit; end

  # POST /equipment/components
  # POST /equipment/components.json
  def create
    @equipment_component = Equipment::Component.new(equipment_component_params)

    respond_to do |format|
      if @equipment_component.save

        User::OperationHistory.create(
          user: current_user,
          action_type: 'create',
          note: "新增設備：#{@equipment_component.hierachy}"
        )

        format.html do
          redirect_to equipment_components_url,
                      notice: 'Component was successfully created.'
        end
        format.json { render :show, status: :created, location: @equipment_component }
      else
        format.html { render :new }
        format.json { render json: @equipment_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equipment/components/1
  # PATCH/PUT /equipment/components/1.json
  def update
    old_hierachy = @equipment_component.hierachy

    respond_to do |format|
      User::OperationHistory.create(
        user: current_user,
        action_type: :update,
        note: "修改設備：#{old_hierachy} -> #{@equipment_component.hierachy}"
      )

      if @equipment_component.update(equipment_component_params)
        format.html do
          redirect_to @equipment_component, notice: 'Component was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @equipment_component }
      else
        format.html { render :edit }
        format.json { render json: @equipment_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment/components/1
  # DELETE /equipment/components/1.json
  def destroy
    hierachy = @equipment_component.hierachy
    @equipment_component.destroy

    User::OperationHistory.create(
      user: current_user,
      action_type: :destroy,
      note: "刪除設備組件：#{hierachy}"
    )
    respond_to do |format|
      format.html do
        redirect_to equipment_components_url, notice: 'Component was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_equipment_component
    @equipment_component = Equipment::Component.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def equipment_component_params
    params.require(:equipment_component).permit(:group_id, :name, :code, :inspection_form_name)
  end
end
