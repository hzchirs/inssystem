class ApiController < ApplicationController
  ALL_ID = 9999
  def get_layer2
    if layer2_first?
      render(json: [{ id: ALL_ID, name: '全部' }].concat(Area.select(:id, :name).where(owner_id: params[:layer1_area_id]).to_a)) && return
    elsif equ_component_first?
      layer2_areas_ids =
        if params[:equ_component_id].to_i == ALL_ID
          Area
            .find(params[:layer1_area_id])
            .equipment_components.map { |equ| 
              equ.layer2_areas
                .filter { |layer2_area| layer2_area.owner_id == params[:layer1_area_id].to_i }
                .map(&:id)
            }.flatten
            .compact
            .uniq
        else
          Equipment::Component.find(params[:equ_component_id]).layer2_areas.pluck(:id)
        end
      

      render(json: [{ id: ALL_ID, name: '全部' }].concat(Area.select(:id, :name).where(id: layer2_areas_ids).to_a))
    end
  end

  def get_equ_component
    if equ_component_first?
      render(json: [{ id: ALL_ID, name: '全部' }].concat(Area.find(params[:layer1_area_id]).equipment_components.uniq.to_a)) && return
    elsif layer2_first?
      equ_components = 
        if params[:layer2_area_id].to_i == ALL_ID
          Area.find(params[:layer1_area_id]).equipment_components.uniq
        else
          Area.find(params[:layer2_area_id]).equipment_components.uniq
        end

      render(json: [{ id: ALL_ID, name: '全部' }].concat(equ_components).to_a) && return
    end
  end

  private

    def layer2_first?
      !!params[:layer2_first]
    end

    def equ_component_first?
      !!params[:equ_component_first]
    end
end
