# frozen_string_literal: true

class QrcodeDownloadController < ApplicationController
  def index
    qrcode = RQRCode::QRCode.new(params[:target_url])

    png = qrcode.as_png(
      bit_depth: 1,
      border_modules: 1,
      color_mode: ChunkyPNG::COLOR_GRAYSCALE,
      color: 'black',
      file: nil,
      fill: 'white',
      module_px_size: 6,
      resize_exactly_to: false,
      resize_gte_to: false,
      size: 300
    )

    send_data png, filename: 'qrcode.png'
  end
end
