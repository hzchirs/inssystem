class User::OperationHistoriesController < ApplicationController
  before_action :set_user_operation_history, only: [:show, :edit, :update, :destroy]

  # GET /user/operation_histories
  # GET /user/operation_histories.json
  def index
    query = User::OperationHistory

    if params[:from] && params[:to]
      year, month, day = params[:from].values_at(:year, :month, :day).map(&:to_i)

      @from = Time.zone.parse("#{year}-#{month}-#{day}")

      year, month, day = params[:to].values_at(:year, :month, :day).map(&:to_i)

      @to = Time.zone.parse("#{year}-#{month}-#{day}")

      query = query.where(created_at: @from..@to)
    end

    if params[:action_type] && params[:action_type] != 'all'
      query = query.where(action_type: params[:action_type])
    end

    @user_operation_histories = query.order(created_at: :desc).page(params[:page]).per(20)
  end

  # GET /user/operation_histories/1
  # GET /user/operation_histories/1.json
  def show
  end

  # GET /user/operation_histories/new
  def new
    @user_operation_history = User::OperationHistory.new
  end

  # GET /user/operation_histories/1/edit
  def edit
  end

  # POST /user/operation_histories
  # POST /user/operation_histories.json
  def create
    @user_operation_history = User::OperationHistory.new(user_operation_history_params)

    respond_to do |format|
      if @user_operation_history.save
        format.html { redirect_to @user_operation_history, notice: 'Operation history was successfully created.' }
        format.json { render :show, status: :created, location: @user_operation_history }
      else
        format.html { render :new }
        format.json { render json: @user_operation_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user/operation_histories/1
  # PATCH/PUT /user/operation_histories/1.json
  def update
    respond_to do |format|
      if @user_operation_history.update(user_operation_history_params)
        format.html { redirect_to @user_operation_history, notice: 'Operation history was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_operation_history }
      else
        format.html { render :edit }
        format.json { render json: @user_operation_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user/operation_histories/1
  # DELETE /user/operation_histories/1.json
  def destroy
    @user_operation_history.destroy
    respond_to do |format|
      format.html { redirect_to user_operation_histories_url, notice: 'Operation history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_operation_history
      @user_operation_history = User::OperationHistory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_operation_history_params
      params.require(:user_operation_history).permit(:user_id, :action_type, :note, :operated_at)
    end
end
