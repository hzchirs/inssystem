# frozen_string_literal: true

class AreasController < ApplicationController
  before_action :set_area, only: %i[show edit update destroy]
  before_action :authenticate_admin!, except: %i[guide index map_image]

  # GET /areas
  # GET /areas.json
  def index
    if params[:get_equ_options] && params[:layer2_area_id] && params[:layer1_area_id]
      options = 
        if params[:layer2_area_id].to_i.zero?
          Area.find(params[:layer1_area_id]).equipment_components
        else
          Area.find(params[:layer2_area_id]).equipment_components
        end
      render(json: options) && return
    end

    if params[:get_options] && params[:layer1_area_id]
      area1 = Area.find(params[:layer1_area_id])

      render(json: { 
        layer2: Area.select(:id, :name).where(owner: area1),
        equipment_components: area1.equipment_components
      }) && return
    end

    if params[:get_full_data] && params[:layer1_area_id] && params[:layer2_area_id]
      if params[:layer2_area_id].to_i == 0
        render(json: Area.where(owner_id: params[:layer1_area_id]).map do |area|
          {
            name: area.name,
            owner: area.owner&.name,
            map_image_url: area.map_image_url ? map_image_area_url(area) : nil
          }
        end) && return
      else
        area = Area.find(params[:layer2_area_id])
        render(json: [{ name: area.name, owner: area.owner&.name, map_image_url: area.map_image_url ? map_image_area_url(area) : nil }]) && return
      end
    end

    @layer1_areas = Area.layer1
    @other_areas = Area.includes(:owner).where.not(id: @layer1_areas)
  end

  def guide
    @layer1_areas = Area.select(:id, :name, :category_id).layer1
    @layer2_areas = Area.select(:id, :name, :category_id).where(owner_id: @layer1_areas.first.id)
  end

  # GET /areas/1
  # GET /areas/1.json
  def show
    respond_to do |format|
      format.html
      format.json { render json: { name: @area.name, owner: @area.owner&.name, map_image_url: @area.map_image_url } }
    end
  end

  # GET /areas/new
  def new
    @area = Area.new
    @has_owner = params[:has_owner]
  end

  # GET /areas/1/edit
  def edit; end

  # POST /areas
  # POST /areas.json
  def create
    @area = Area.new(area_params)

    if params[:area][:english_name] && params[:area][:code]
      category = Area::Category.create(
        name: params[:area][:name],
        english_name: params[:area][:english_name],
        code: params[:area][:code]
      )

      @area.category = category
    end

    respond_to do |format|
      if @area.save
        note = if @area.layer1?
                 "新增工區：#{@area.name}"
               else
                 "新增設施：#{@area.owner&.name}-#{@area.name}"
               end

        User::OperationHistory.create(
          user: current_user,
          action_type: 'create',
          note: note
        )

        format.html { redirect_to areas_url, notice: 'Area was successfully created.' }
        format.json { render :show, status: :created, location: @area }
      else
        format.html { render :new }
        format.json { render json: @area.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /areas/1
  # PATCH/PUT /areas/1.json
  def update
    render 'edit' and return unless params[:area]

    note = if @area.layer1?
             '修改工區：'
           else
             '修改設施：'
           end

    note += @area.hierachy

    respond_to do |format|
      if @area.update(area_params)
        if %w[name owner_id number].any? { |column| column.in? @area.saved_changes.keys }
          note += ' -> '
          note += @area.hierachy

          User::OperationHistory.create(
            user: current_user,
            action_type: :update,
            note: note
          )
        end

        if ['map_image_data'].in? @area.saved_changes.keys
          note.concat += "更新 #{@area.hierachy} 的地圖"

          User::OperationHistory.create(
            user: current_user,
            action_type: :upload_image,
            note: note
          )
        end

        format.html { redirect_to edit_area_path(@area), notice: 'Area was successfully updated.' }
        format.json { render :show, status: :ok, location: @area }
      else
        format.html { render :edit, notice: 'Update Failed!!!!!' }
        format.json { render json: @area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /areas/1
  # DELETE /areas/1.json
  def destroy
    hierachy = @area.hierachy
    ch_type = @area.ch_type

    @area.destroy

    User::OperationHistory.create(
      user: current_user,
      action_type: :destroy,
      note: "刪除#{ch_type}：#{hierachy}"
    )
    respond_to do |format|
      format.html { redirect_to areas_url, notice: 'Area was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_map
    @area = Area.find(params[:area_id])
    @area.update(map_image: nil)

    redirect_to edit_area_path(@area)
  end

  def map_image
    area = Area.find(params[:id])

    redirect_to area.map_image_url
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_area
    @area = Area.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def area_params
    params.require(:area).permit(:name, :number, :category_id, :owner_id, :map_image)
  end
end
