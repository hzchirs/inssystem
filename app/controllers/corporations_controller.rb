# frozen_string_literal: true

class CorporationsController < ApplicationController
  before_action :set_corporation, only: %i[show edit update destroy]

  # GET /corporations
  # GET /corporations.json
  def index
    @corporations = Corporation.all
  end

  # GET /corporations/1
  # GET /corporations/1.json
  def show; end

  # GET /corporations/new
  def new
    @corporation = Corporation.new
  end

  # GET /corporations/1/edit
  def edit; end

  # POST /corporations
  # POST /corporations.json
  def create
    @corporation = Corporation.new(corporation_params)

    respond_to do |format|
      if @corporation.save
        User::OperationHistory.create!(
          user: current_user,
          action_type: :create,
          note: "新增單位：#{@corporation.name}"
        )

        format.html { redirect_to users_url, notice: 'Corporation was successfully created.' }
        format.json { render :show, status: :created, location: @corporation }
      else
        format.html { render :new }
        format.json { render json: @corporation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /corporations/1
  # PATCH/PUT /corporations/1.json
  def update
    respond_to do |format|
      old_name = @corporation.name

      if @corporation.update(corporation_params)
        if old_name != @corporation.name
          User::OperationHistory.create!(
            user: current_user,
            action_type: :update,
            note: "修改單位：#{old_name} -> #{@corporation.name}"
          )
        end

        format.html { redirect_to users_url, notice: 'Corporation was successfully updated.' }
        format.json { render :show, status: :ok, location: @corporation }
      else
        format.html { render :edit }
        format.json { render json: @corporation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /corporations/1
  # DELETE /corporations/1.json
  def destroy
    User::OperationHistory.create!(
      user: current_user,
      action_type: :destroy,
      note: "刪除單位：#{@corporation.name}"
    )

    @corporation.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Corporation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_corporation
    @corporation = Corporation.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def corporation_params
    params.require(:corporation).permit(:name)
  end
end
