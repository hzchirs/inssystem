# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :prevent_not_signed_in_user_visit_pages_except_sign_in

  helper_method :current_user, :user_signed_in?

  private

  def authenticate_admin!
    redirect_to root_url unless current_user.admin? || current_user.super_admin?
  end

  def current_user
    @current_user ||= session[:user_id] &&
                      User.find(session[:user_id])
  end

  def user_signed_in?
    current_user.present?
  end

  def prevent_not_signed_in_user_visit_pages_except_sign_in
    return if user_signed_in?
    return if controller_name == 'sessions'
    return if action_name == 'pdf'

    redirect_to new_session_path
  end
end
