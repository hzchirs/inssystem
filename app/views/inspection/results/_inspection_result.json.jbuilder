json.extract! inspection_result, :id, :item_id, :result, :created_at, :updated_at
json.url inspection_result_url(inspection_result, format: :json)
