json.extract! inspection_history, :id, :equipment_id, :year, :uploaded_at, :serial_number, :maintain_type, :results, :created_at, :updated_at
json.url inspection_history_url(inspection_history, format: :json)
