json.extract! equipment_item, :id, :metadata_id, :area_id, :name, :created_at, :updated_at
json.url equipment_item_url(equipment_item, format: :json)
