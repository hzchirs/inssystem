json.extract! area, :id, :name, :owner_id, :created_at, :updated_at
json.url area_url(area, format: :json)
