json.extract! user_operation_history, :id, :user_id, :action_type, :note, :operated_at, :created_at, :updated_at
json.url user_operation_history_url(user_operation_history, format: :json)
