json.extract! corporation, :id, :name, :created_at, :updated_at
json.url corporation_url(corporation, format: :json)
